################# Security Groups #############
resource "aws_security_group" "demo-alb-sg" {
    name        = "DemoALBSG"
    vpc_id      = "${aws_vpc.demo-vpc.id}"

    # Allow HTTP port 80 access from any source
    ingress { 
      from_port = 80
      to_port = 80
      protocol = "tcp" 
      cidr_blocks = ["0.0.0.0/0"] 
    } 
    egress { 
        from_port = 0 
        to_port = 0 
        protocol = "-1" 
        cidr_blocks = ["0.0.0.0/0"] 
    } 

    tags {
        Name = "DemoALBSG"
    }
}

################# Application Load Balancer #############
# This is the top level component in the architecture. 
# Handles the incoming traffic, offloads SSL and balances the load.
resource "aws_lb" "demo-alb" {  
  name                  = "DemoALB"  
  load_balancer_type    = "application"
  subnets               = ["${aws_subnet.public-subnet-a.id}", "${aws_subnet.public-subnet-b.id}"]
  security_groups       = ["${aws_security_group.demo-alb-sg.id}"]
  internal              = false
  ip_address_type       = "ipv4"
  
  tags {    
    Name    = "DemoALB"    
  }
}

################# ALB Listener & Listener Rules #############
# Listeners are assigned a specific port to keep an ear out for incoming traffic.
resource "aws_alb_listener" "alb_listener" {  
  load_balancer_arn = "${aws_lb.demo-alb.arn}"  
  port              = "80"  
  protocol          = "HTTP"
  
  default_action {    
    target_group_arn = "${aws_alb_target_group.demo-alb-tg.arn}"
    type             = "forward"  
  }
}
#  Each listener can have many rules as an alternative to the default action.
# This means we can route traffic to different places based on two conditions; the path and/or the host.
# **NOTE** This is not needed in our initial setup so we can comment.
# resource "aws_alb_listener" "demo-alb-http-listener-rule" {
#   depends_on   = ["aws_alb_target_group.demo-alb-tg"]  
#   load_balancer_arn = "${aws_alb_listener.alb_listener.arn}"  
#   priority     = "${var.priority}"   
#   action {    
#     type             = "forward"    
#     target_group_arn = "${aws_alb_target_group.alb_target_group.id}"  
#   }   
#   condition {    
#     field  = "path-pattern"    
#     values = ["${var.alb_path}"]  
#   }
# }

################# ALB Routing - Target Group #############
# Target groups are essentially the end point of the ALB architecture....
# ...When the listener rule matches a pattern for a request it gets forwarded to the correlating target group
resource "aws_alb_target_group" "demo-alb-tg" {  
  name     = "DemoALBTG"  
  port     = "80"  
  protocol = "HTTP"  
  vpc_id   = "${aws_vpc.demo-vpc.id}"   

  tags {    
    name = "DemoALBTG"    
  }   

  stickiness {    
    type            = "lb_cookie"
    enabled         = false
  }   

  health_check {    
    healthy_threshold   = 2    
    unhealthy_threshold = 10    
    timeout             = 5    
    interval            = 10    
    path                = "/"    
    port                = "80"  
  }
}

################# Assign Autoscaling Group to Target Group #############
# Now we have a target group we need to assign something to it. This is done through target group attachments
resource "aws_autoscaling_attachment" "demo-attach-asg-to-alg-tg" {
  alb_target_group_arn   = "${aws_alb_target_group.demo-alb-tg.arn}"
  autoscaling_group_name = "${aws_autoscaling_group.DemoASG.id}"
}
