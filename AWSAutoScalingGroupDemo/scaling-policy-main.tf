################# Scale Out EC2 instance scaling policy #############

# Scaling policy. Add 1 instance if alarm is triggered
resource "aws_autoscaling_policy" "demo-scaleout-cpu-policy" {
count = "${var.scaling_policy_enabled == "true" ? 1 : 0}"
name = "DemoScaleOutCPUPolicy"
autoscaling_group_name = "${aws_autoscaling_group.DemoASG.name}"
adjustment_type = "ChangeInCapacity"
scaling_adjustment = "1"
cooldown = "150"
policy_type = "SimpleScaling"
}
# Cloudwatch alarm if CPU utilisation is >= 40%
resource "aws_cloudwatch_metric_alarm" "demo-scaleout-cpu-alarm" {
count = "${var.scaling_policy_enabled == "true" ? 1 : 0}"
alarm_name = "DemoScaleOutCPUAlarm"
alarm_description = "DemoScaleOutCPUAlarm"
comparison_operator = "GreaterThanOrEqualToThreshold"
evaluation_periods = "1"
metric_name = "CPUUtilization"
namespace = "AWS/EC2"
period = "60"
statistic = "Average"
threshold = "40"
dimensions = {
"AutoScalingGroupName" = "${aws_autoscaling_group.DemoASG.name}"
}
actions_enabled = true
alarm_actions = ["${aws_autoscaling_policy.demo-scaleout-cpu-policy.arn}"]
}

################# Scale In EC2 instance scaling policy #############

# Scaling policy. Remove 1 instance if alarm is triggered
resource "aws_autoscaling_policy" "demo-scalein-cpu-policy" {
count = "${var.scaling_policy_enabled == "true" ? 1 : 0}"    
name = "DemoScaleInCPUPolicy"
autoscaling_group_name = "${aws_autoscaling_group.DemoASG.name}"
adjustment_type = "ChangeInCapacity"
scaling_adjustment = "-1"
cooldown = "150"
policy_type = "SimpleScaling"
}

# Cloudwatch alarm if CPU utilisation is <= 20%
resource "aws_cloudwatch_metric_alarm" "demo-scalein-cpu-alarm" {
count = "${var.scaling_policy_enabled == "true" ? 1 : 0}"
alarm_name = "DemoScaleInCPUAlarm"
alarm_description = "DemoScaleInCPUAlarm"
comparison_operator = "LessThanOrEqualToThreshold"
evaluation_periods = "1"
metric_name = "CPUUtilization"
namespace = "AWS/EC2"
period = "60"
statistic = "Average"
threshold = "20"
dimensions = {
"AutoScalingGroupName" = "${aws_autoscaling_group.DemoASG.name}"
}
actions_enabled = true
alarm_actions = ["${aws_autoscaling_policy.demo-scalein-cpu-policy.arn}"]
}
