################ VPC #################
resource "aws_vpc" "demo-vpc" {
  cidr_block       = "${var.main_vpc_cidr}"
  instance_tenancy = "default"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags {
    Name = "DemoVPC"
  }
}

 ################# Subnets #############
resource "aws_subnet" "public-subnet-a" {
  vpc_id     = "${aws_vpc.demo-vpc.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "${var.availability_zone1}"

  tags {
    Name = "PublicSubnetA"
    }
}
resource "aws_subnet" "public-subnet-b" {
  vpc_id     = "${aws_vpc.demo-vpc.id}"
  cidr_block = "10.0.2.0/24"
  availability_zone = "${var.availability_zone2}"

  tags {
    Name = "PublicSubnetB"
  }
}
resource "aws_subnet" "private-subnet-c" {
  vpc_id     = "${aws_vpc.demo-vpc.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "${var.availability_zone3}"

  tags {
    Name = "PrivateSubnetC"
  }
}

resource "aws_subnet" "private-subnet-d" {
  vpc_id     = "${aws_vpc.demo-vpc.id}"
  cidr_block = "10.0.4.0/24"
  availability_zone = "${var.availability_zone4}"

  tags {
    Name = "PrivateSubnetD"
  }
}
######## IGW ###############
resource "aws_internet_gateway" "demo-igw" {
  vpc_id     = "${aws_vpc.demo-vpc.id}"

  tags {
    Name = "DemoIGW"
  }
}

########### NAT ##############
# resource "aws_eip" "nat" {
# }

# resource "aws_nat_gateway" "main-natgw" {
#   allocation_id = "${aws_eip.nat.id}"
#   subnet_id     = "${aws_subnet.subnet8.id}"

#   tags {
#     Name = "main-nat"
#   }
# }

############# Route Tables ##########

resource "aws_route_table" "demo-public-rt" {
  vpc_id     = "${aws_vpc.demo-vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.demo-igw.id}"
  }

  tags {
    Name = "DemoPublicRT"
  }
}

resource "aws_route_table" "demo-private-rt" {
  vpc_id     = "${aws_vpc.demo-vpc.id}"
  # route {
  #   cidr_block = "0.0.0.0/0"
  #   gateway_id = "${aws_nat_gateway.main-natgw.id}"
  # }

  tags {
    Name = "DemoPrivateRT"
  }
}

######### PUBLIC Subnet assiosation with route table    ######
resource "aws_route_table_association" "subnet-a-public-assoc" {
  subnet_id      = "${aws_subnet.public-subnet-a.id}"
  route_table_id = "${aws_route_table.demo-public-rt.id}"
}
resource "aws_route_table_association" "subnet-b-public-assoc" {
  subnet_id      = "${aws_subnet.public-subnet-b.id}"
  route_table_id = "${aws_route_table.demo-public-rt.id}"
}
########## PRIVATE Subnets assiosation with route table ######
resource "aws_route_table_association" "subnet-c-private-assoc" {
  subnet_id      = "${aws_subnet.private-subnet-c.id}"
  route_table_id = "${aws_route_table.demo-private-rt.id}"
}

resource "aws_route_table_association" "subnet-d-private-assoc" {
  subnet_id      = "${aws_subnet.private-subnet-d.id}"
  route_table_id = "${aws_route_table.demo-private-rt.id}"
}