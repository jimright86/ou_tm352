################# KeyPair to SSH onto instances #############
resource "aws_key_pair" "demo-key-pair" {
  key_name = "DemoEC2KeyPair"
  public_key = "${file("terraform_ec2_key.pub")}"
}

################# Security Groups #############
resource "aws_security_group" "demo-ec2-sg" {
    vpc_id      = "${aws_vpc.demo-vpc.id}"

    # Allow SSH access from any source
    ingress { 
        from_port = 22 
        to_port = 22 
        protocol = "tcp" 
        cidr_blocks = ["0.0.0.0/0"] 
    } 
    
    ingress { 
      from_port = 80
      to_port = 80
      protocol = "tcp" 
      cidr_blocks = ["0.0.0.0/0"] 
    } 
    egress { 
        from_port = 0 
        to_port = 0 
        protocol = "-1" 
        cidr_blocks = ["0.0.0.0/0"] 
    } 

    tags {
        Name = "DemoEC2SG"
    }
}

################# Test EC2 Instance to confirm everything is working #############
# resource "aws_instance" "demo-ec2-instance" {
#   ami                           = "ami-00068cd7555f543d5"
#   instance_type                 = "t3.micro"
#   subnet_id                     = "${aws_subnet.public-subnet-a.id}"
#   availability_zone             = "${var.availability_zone1}"
#   key_name                      = "${aws_key_pair.demo-key-pair.id}" 
#   associate_public_ip_address   = true
#   vpc_security_group_ids        = ["${aws_security_group.demo-ec2-sg.id}"] # a list
# }

################# Launch Template & Auto Scaling Group #############
data "template_file" "user_data" {
  template = <<EOF
#!/bin/bash
yum update -y
yum install -y httpd
yum install -y wget
cd /var/www/html
echo "Hello from AWS from `hostname`" >> index.html
service httpd start

amazon-linux-extras install epel -y
yum install -y stress
EOF
}

resource "aws_launch_template" "demo-lt" {
  name                                  = "DemoLT"
  image_id                              = "ami-00068cd7555f543d5"
  instance_type                         = "t3.micro"
  key_name                              = "${aws_key_pair.demo-key-pair.id}"
  # vpc_security_group_ids                = ["${aws_security_group.demo-ec2-sg.id}"] # a list
  disable_api_termination               = true
  instance_initiated_shutdown_behavior  = "terminate"
  
  network_interfaces {
    security_groups                = ["${aws_security_group.demo-ec2-sg.id}"] # a list
    delete_on_termination = true
    associate_public_ip_address = true
  }

  monitoring {
    enabled = true
  }

  user_data = "${base64encode(data.template_file.user_data.rendered)}"
}

resource "aws_autoscaling_group" "DemoASG" {
  vpc_zone_identifier  = ["${aws_subnet.public-subnet-a.id}", "${aws_subnet.public-subnet-b.id}"]
  # availability_zones = ["${var.availability_zone1}", "${var.availability_zone2}"] # only needed for ELB-classic. Causes an diff error when included for this configuration
  desired_capacity   = 2
  max_size           = 6
  min_size           = 2


  launch_template = {
    id      = "${aws_launch_template.demo-lt.id}"
    version = "${aws_launch_template.demo-lt.latest_version}"
  }

  enabled_metrics = [
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupMaxSize",
    "GroupMinSize",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]
}
