# AWS-Terraform
This provides the Terraform code to create an AWS infrastructure suitable for demonstration of the key AWS components and cloud elasticity. It helps to show how automatic scale-out/scale-in of EC2 instances based on changing load on the instances can be achieved using Auto-Scaling Groups and scaling policies.

![Auto Scaling Architecture](AWS_ASG_Architecture.png)

References and Useful links:
* https://dzone.com/articles/high-availability-vpc-with-terraform
* https://github.com/vineet67sharma/AWS-Terraform

## Dependencies

### Terraform

* Terraform needs to be installed in order to run the various Terraform scripts.

* I've tested this using Terraform version 0.11.*. To install version 0.11:
```
curl -O https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip
unzip terraform_0.11.13_linux_amd64.zip 
sudo mv terraform /usr/local/bin/terraform
```

### AWS user to run terraform

* A user with programmatic access is required to run Terraform. 

* Here's a quick overview of the steps to create this user:
    * In AWS management console create a `terraform_user` which will be used to run Terraform

    * This user should have `Programmatic access` which provides an access key ID and secret access key used by Terraform AWS provider.

    * In the permission settings, select `Attach existing policies directly` and add the `AdministratorAccess` policy.

    * Copy the Access Key ID and Secret Key created and replace these values in the relevant terraform variables in the file `terraform.tfvars`.

### SSH Keypair

* To be able to SSH into the EC2 instances we need to create an AWS key pair. In the Terraform code this is used as part of the configuration of the Auto Scaling Group (see asg-main.tf)

* As part of the setup to run the code we need to generate a SSH keypair. This can be done using the `ssh-keygen` command as follows:
```
ssh-keygen -f terraform_ec2_key
```

* The public key (`terraform_ec2_key.pub`) should then be places in the same folder as the Terraform code.

* We then use the private key when we wish to login to the EC2 instance, e.g:
    ```
    ssh -i terraform_ec2_key ec2-user@<EC2-public-ip-address>
    ```

## Running the code

### Edit `terraform.tfvars`

* Set the various variables in the terraform.tfvars.
    * A preview of the file is shown below
    * The `aws_access_key` and `aws_secret_key` need to be changed based on your AWS user created above.
    * The `scaling_policy_enabled` flag, if set to true enables the creation of Cloud Watch Alarms and Scaling Policies to Scale Out and Scale in the number of EC2 instances based on CPU Utilisation.
    * The others can be optionally changed if you wish to create the infrastructure in a different location.

````
aws_access_key = "xxxxx"
aws_secret_key = "yyyyyyyy"
aws_region = "us-east-1"
availability_zone1 = "us-east-1a"
availability_zone2 = "us-east-1b"
availability_zone3 = "us-east-1c"
availability_zone4 = "us-east-1d"
scaling_policy_enabled = "false"
````

### Initialise terraform

```
$ terraform init
```
* Initialise the Terraform working directory by creating initial files and download any required modules.


### Review the changes to the architecture

```
$ terraform plan
```
* This will review the changes and additions to the infrastructure. It also validates the code to see if there are any issues with the configurations.

### Apply to create the infrastructure

```
$ terraform apply
```
* Build or change the infrastructure.

## Demo Auto-scaling functionality

The code applies a scaling policy to the autoscaling group which will add an EC2 instance if the CPU utilisation is >= 40% and will remove an EC2 instance if the utilisation is <= 20%. Note the minimum number of EC2 instances is set to 2 (in the `asg-main.tf` file in the `"aws_autoscaling_group"` -> `"DemoASG"` resource).

To demonstrate the autoscaling group functionality we will connect to one of the EC2 instances and run a stress test. An example of the commands to run are shown below:

```
# Connect to the EC2 instance
$ ssh -i terraform_ec2_key ec2-user@<EC2-public-ip-address>

# Install the stress command from epel repo
$ sudo amazon-linux-extras install epel -y
$ sudo yum install -y stress

# Run the stress test with:
$ stress --cpu 2 --timeout 300

```

The CPU load should increase when this command is launched. After a few minutes watch the number of instances increase.
Kill the stress command (Ctrl-C) and after a while the number of instances should return to the minimum value specified in the auto-scaling group.
