variable "aws_access_key" {}
variable "aws_secret_key" {}

variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "us-east-1"
}
variable "availability_zone1" {
    description = "Avaialbility Zones"
    default = "us-east-1a"
}

variable "availability_zone2" {
    description = "Avaialbility Zones"
    default = "us-east-1b"
}
variable "availability_zone3" {
    description = "Avaialbility Zones"
    default = "us-east-1c"
}

variable "availability_zone4" {
    description = "Avaialbility Zones"
    default = "us-east-1d"
}
variable "main_vpc_cidr" {
    description = "CIDR of the VPC"
    default = "10.0.0.0/16"
}
variable "scaling_policy_enabled" {
    type = "string"
}